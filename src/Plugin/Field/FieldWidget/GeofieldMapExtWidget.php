<?php

namespace Drupal\geofield_map_ext\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\geofield_map\Plugin\Field\FieldWidget\GeofieldMapWidget;
use Drupal\geofield_map_ext\GeofieldMapFieldInterface;

/**
 * Plugin implementation of the 'geofield_map_ext' widget.
 *
 * @FieldWidget(
 *   id = "geofield_map",
 *   label = @Translation("Geofield Map Ext"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeofieldMapExtWidget extends GeofieldMapWidget implements ContainerFactoryPluginInterface, GeofieldMapFieldInterface {
  /**
   * The Map Library Plugin Manager service.
   *
   * @var \Drupal\geofield_map_ext\MapLibraryPluginManager
   */
  protected static $mapLibraryManager;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = [];

    /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
    foreach (self::getMapLibraryManager()->getDefinitions() as $map_library_plugin_definition) {
      $map_library_plugin = self::getMapLibraryManager()->createInstance($map_library_plugin_definition['id']);
      $map_library_plugin->mapWidgetDefaultSettings($default_settings);
    }

    return $default_settings + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
    foreach (self::getMapLibraryManager()->getDefinitions() as $map_library_plugin_definition) {
      $map_library_plugin = self::getMapLibraryManager()->createInstance($map_library_plugin_definition['id']);
      $map_library_plugin->mapWidgetSettingsForm($this, $elements);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();
    if (self::getMapLibraryManager()->hasDefinition($settings['map_library'])) {
      /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
      $map_library_plugin = self::getMapLibraryManager()->createInstance($settings['map_library']);
      if (isset($map_library_plugin)) {
        $map_library_plugin->mapWidgetSettingsSummary($this, $summary);
      }
    }

    return $summary;
  }

  /**
   * Implements \Drupal\field\Plugin\Type\Widget\WidgetInterface::formElement().
   *
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $form_element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element = $form_element['value'];

    // Specific Element type required to inject geo_settings and GEO specific
    // libraries.
    $element['#type'] = 'geofield_map_ext';

    if (self::getMapLibraryManager()->hasDefinition($element['#map_library'])) {
      /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
      $map_library_plugin = self::getMapLibraryManager()->createInstance($element['#map_library']);
      if (isset($map_library_plugin)) {
        $map_library_plugin->mapWidgetFormElement($this, $element);
      }
    }

    return ['value' => $element];
  }

  /**
   * @return mixed
   */
  public function getFieldName() {
    return $this->fieldDefinition->getName();
  }

  /**
   * @return \Drupal\Core\Utility\LinkGeneratorInterface
   */
  public function getLink() {
    return $this->link;
  }

  /**
   * Fetchs the Map Library Plugin Manager service.
   * @return \Drupal\geofield_map_ext\MapLibraryPluginManager|mixed
   */
  protected static function getMapLibraryManager() {
    if (empty(self::$mapLibraryManager)) {
      self::$mapLibraryManager = \Drupal::service('plugin.manager.map_library_plugin');
    }
    return self::$mapLibraryManager;
  }

}
